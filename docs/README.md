StoryMap
========

Our vision for a unified product in a user story map (see [The New User Story
Backlog is a Map](https://www.jpattonassociates.com/the-new-backlog/) for more
information on user story maps).

[![Story Map](./product-story-map.png)](https://gitlab.com/LibreFoodPantry/StoryMap/raw/master/docs/product-story-map.png ":ignore")

This project is a LibreFoodPantry project. As such, we share its policies,
practices, and infrastructure. Please visit LibreFoodPantry.org for more
information.

- Source code licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)
- Content licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
- [Code of Conduct](https://librefoodpantry.org/#/overview/code-of-conduct/)
- [Discord server](https://discord.gg/PRth8YK)
- [Source code and issue tracker](https://gitlab.com/LibreFoodPantry/StoryMap)
- [What's New](./CHANGELOG.md)


---

Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
